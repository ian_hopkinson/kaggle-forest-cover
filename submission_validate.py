#!/usr/bin/env python
# encoding: utf-8
from __future__ import unicode_literals

import codecs
import csv
import sys

from collections import Counter


def check(argv=None):
    if argv is None:
        argv = sys.argv
        arg = argv[1:]
    else:
        arg = [argv]

    if len(arg) == 0:
        print("Available commandlines:\n")
        print("submission_validate.py [filename]")
        return
    else:
        submission_file = arg[0]
    print("Validating {}".format(submission_file))
    # 
    ids = set()
    type_histogram = Counter()
    validated = True
    # Read submission file
    row_count = 0
    with open(submission_file, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        header = reader.next()
        for row in reader:
            row_count += 1
            ids.add(int(row[0]))
            type_histogram[row[1]] += 1

    # Check row_count
    if row_count != 565892:
        print("Submission failed on length, got {}, expected 565,892".format(row_count))
        validated = False
    
    # Check header is 'Id,Cover_Type'
    if header != ['Id', 'Cover_Type']:
        print("Sumbission failed on header")
        validated = False

    # Check number of lines is 565,892
    if len(ids) != 565892:
        print("Submission failed on unique id count, got {}, expected 565,892".format(len(ids)))
        validated = False
    # Check Ids are 15121 to 581012
    for i in range(15121, 565893):
        if i not in ids:
            print("Submission failed with id = {} not present".format(i))
            validated = False
            break
    # Count cover types
    # print type_histogram
    
    if __name__ == "__main__": 
        print("Submission validated: {}".format(validated))
        max_cnt = max([i for i in type_histogram.values()])
        for i in range(1,8):
            print("{0} : {1:0.2f} ".format(i, float(type_histogram[str(i)])/float(max_cnt)))

    return validated

if __name__ == "__main__":
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    check()
    