#!/usr/bin/env python
# encoding: utf-8
from __future__ import unicode_literals

import codecs
import sys
import numpy

from forest_type_classifier import load_forest_data
from utilities import sort_dict_by_value, pretty_print_dict

def main():
    print("********************\n*Training data analysis*\n********************")
    report = {}
    # Load data and apply standard process
    train_data, target, target_names, feature_names, train_Id = load_forest_data("input/train.csv")
    report['feature_names'] = sort_dict_by_value(feature_names)
    pretty_print_dict(report['feature_names'])
    # Test_data needs doing in chunks
    # test_data, _, _, _, test_Id = load_forest_data("input/test.csv")
    # For each categorical field apply the type field counter

    # For each continuous field apply the histogram function
    elevation = train_data[:, feature_names['Elevation']]
    # print(numpy.shape(train_data))
    n_bins = 10
    step = 200.0
    start = 1800.0
    bins = [start + x * step for x in range(0,12)]
    print(bins)
    print("Number of data points: {}".format(len(elevation)))
    print("Min: {}, Max: {}".format(min(elevation), max(elevation)))
    train_hist, _ = numpy.histogram(elevation, bins=bins)

    print("Histogram: {}".format(train_hist))
    print("Bins: {}".format(bins))
    print("Bin width: {}".format(bins[1] - bins[0]))
    # d = numpy.digitize(elevation, bins) 

    # We have to handle the test data in chunks
    print("********************\n*Test data analysis*\n********************")
    chunk_size = 50000
    still_data = True
    offset = 0

    global_min = []
    global_max = []
    total_datapoints = 0
    global_hist = [0]*11
    while still_data:
        print("Processing chunk at offset = {}".format(offset))
        # load the test data - need to process in the same way as for building classifier
        test_data, _, _, _, _ = load_forest_data("input/test.csv", offset=offset, limit=chunk_size)
        if len(test_data)==0:
            still_data = False
            print("Exiting with Id length of zero")
            break
        elevation = test_data[:, feature_names['Elevation']]
        global_min.append(min(elevation))
        global_max.append(max(elevation))
        total_datapoints += len(elevation)
        hist, _ = numpy.histogram(elevation, bins=bins)
        print(hist)
        global_hist += hist
        print("data length: {}".format(len(test_data)))
        
        offset = offset + chunk_size

    print("Number of data points: {}".format(total_datapoints))
    print("Min: {}, Max: {}".format(min(global_min), max(global_max)))

    print("Histogram: {}".format(global_hist))
    print("Bins: {}".format(bins))
    print("Bin width: {}".format(bins[1] - bins[0]))

if __name__ == "__main__":
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    main()
    