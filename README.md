# Kaggle forest cover Machine Learning competition

Web page for this challenge is here:

https://www.kaggle.com/c/forest-cover-type-prediction

This repo includes the training data but not the 72MB test data, this should be 
downloaded and unzipped to the directory `input/`

# Usage

* `data_convert.py` - converts test.csv or train.csv into a form more convenient for Tableau (i.e. categorical data encoded as int not binary)
* `forest_type_classifier.py` - makes and saves a classifier, type determined by commandline argument, run with no arguments to see options
* `classify_for_submission.py` - run classifier on test set, commandline includes classifier pickle
* `submission_validate.py` - do some simple checks on a file named on the commandline
* `utilities.py` - contains functions for interacting with Git and writing a dictionary

# TODO


* Process test file in chunks and tune classify for each chunk
* Inspect trees
* Tune parameters again (maybe using gridsearch)
* Try reducing the number of Soil Types or cutting completely - this reduces score but only slightly -DONE
* subsample training set based on elevation - DONE
* Onehot field_histogram assumes that data are 1 or 0 which they aren't post rescaling - DONE
* Try tuning on input features not output classes - start with Wilderness area - DONE
* Make a histogram function - perhaps handling onehot categories properly - DONE
* Log features used and weightings - DONE
* Look at feature importances - DONE slightly fiddly
* Look at output class histogram - DONE
* Tune training set to same distribution as Test - DONE but not optimise
* Automated sense checking of submission data (nlines=565,892, header='Id,Cover_Type') - DONE
* Command line for classify for submission - name of classifier pickle - DONE
* Logging of submission runs - DONE
* Add interaction with git - in first instance don't run on uncommitted changes - DONE
* Try KNeighbours classifier and LinearSVC "as is" for submission - DONE
* Apply classifier to test set - DONE
* Submit first try - DONE
* Make sure we conserve column order for use with test - DONE in forest_type_classifier
* Analyse test set to check it is distributed the same way as the training set - DONE
* Store classifier - DONE
* Store output from building classifier - DONE
* Tune parameters of favoured classifiers - DONE (a bit)
* Work out how to make a submission - DONE
* Add the words in the soil type as features - forum suggests this is pointless - DONE
* Mechanise the selection of features - DONE

# Notes on next task

numpy has digitize and histogram functions:
http://stackoverflow.com/questions/2275924/how-to-get-data-in-a-histogram-bin

hist, edges = numpy.histogram(data, bins=3)
d = numpy.digitize(A, bins) 

http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.ExtraTreesClassifier.html
http://scikit-learn.org/stable/modules/tree.html
We can export classifiers to dot:
~~~~
>>> from sklearn.externals.six import StringIO  
>>> import pydot 
>>> dot_data = StringIO() 
>>> tree.export_graphviz(clf, out_file=dot_data) 
>>> graph = pydot.graph_from_dot_data(dot_data.getvalue()) 
>>> graph.write_pdf("iris.pdf") 
~~~~

# Notes on the Kaggle Forest Cover challenge

scikit-learn considers all inputs to be Gaussian, continuous

Inputs should be scaled to have mean of zero, and variance +/-1

Categorical data are unraveled to a set of binary features

Reports on classifiers change with each run

Data were imported as strings and therefore numerical features were converted to
large categorical features. Undoing this error meant we could use all the data
without memory errors and our F1 recall improved.

Dropping the Hillshade measures seems to give a small benefit

Pickling the classifier is not generally a problem but for some particular parameters
on the ExtraTrees classifier it is a problem. Reducing n_estimators from 1000 to 100
solved the problem.

# Interacting with git from Python

This thread discusses some issues, it seems subprocess is pretty good:

http://stackoverflow.com/questions/1456269/python-git-module-experiences

Commands to implement:

* Am I up to date? git diff [filename] is empty if you are up to date
* What is my SHA? git rev-parse HEAD, git rev-parse --short HEAD
* Commit current state? 

# Notes on the test set

* The Aspect seems similar
* Elevation distribution is moderately different - test set is biased to higher altitudes
* Soil type distributions are a bit different
* Wilderness area distributions are a bit different - mainly Rawah and Comanche for Test

From http://www.kaggle.com/c/forest-cover-type-prediction/forums/t/10708/validation-versus-lb-score

The test set is highly biased to classes 1 and 2:

Class|               #
-----|----------------
1    |  211,229 
2    |  234,732 
3    |  36,391 
4    |  1,974 
5    |  28,176 
6    |  23,780 
7    |  29,610

# Submission instructions

Submission File

Your submission file should have the observation Id and a predicted cover type (an integer between 1 and 7, inclusive). 
The file should contain a header and have the following format:

~~~~
Id,Cover_Type
15121,1
15122,1
15123,1
...
~~~~

# Submission record

2015-04-09  ExtraTrees-25c0d66_25c0d66.csv scroed 0.75796, removed soil type features

2015-04-08  ExtraTrees-9a6dcc5_9a6dcc5.csv scored 0.75580, used an elevation bias
new_dist = {"bins":bins,
            "dist":[0.3, 0.3, 0.3, 0.3, 1.0, 1.0, 1.0, 1.0, 0.3, 0.3, 0.3]}

2015-04-08  ExtraTrees-ea2985e_ea2985e.csv scored 0.76670, used an elevation bias
new_dist = {"bins":bins,
            "dist":[0.1, 0.1, 0.1, 0.3, 1.0, 1.0, 1.0, 1.0, 0.3, 0.3, 0.3]}

2015-04-08  ExtraTrees-3d315f6_3d315f6.csv scored 0.76329, used an elevation bias
new_dist = {"bins":bins,
                "dist":[0.1, 0.1, 0.1, 0.3, 1.0, 1.0, 1.0, 1.0, 0.3, 0.3, 0.3]}

2015-04-08  ExtraTrees-32f7026_32f7026.csv scored 0.76506, used an elevation bias
new_dist = {"bins":bins,
                "dist":[0.3, 0.1, 0.1, 0.3, 1.0, 1.0, 1.0, 1.0, 0.3, 0.3, 0.3]}

2015-04-02  ExtraTrees-29c4598_29c4598.csv scored 0.75286, used class_weights="auto"
new_dist = {"Wilderness_Area1": 1.0, "Wilderness_Area2": 0.1,"Wilderness_Area3": 1.0, "Wilderness_Area4": 0.1}

2015-04-02  ExtraTrees-e550822_e550822.csv scored 0.75376, used all of training set for training this had
new_dist = {"Wilderness_Area1": 1.0, "Wilderness_Area2": 0.1,"Wilderness_Area3": 1.0, "Wilderness_Area4": 0.1}

2015-04-02  ExtraTrees-28d5176_28d5176.csv scored 0.75376, used all of training set for training this had
new_dist = {"Wilderness_Area1": 1.0, "Wilderness_Area2": 0.1,"Wilderness_Area3": 1.0, "Wilderness_Area4": 0.1}

2015-04-02  ExtraTrees-a9a1bd3_a9a1bd3.csv scored 0.71646, this had
new_dist = {"Wilderness_Area1": 1.0, "Wilderness_Area2": 0.9,"Wilderness_Area3": 0.55, "Wilderness_Area4": 0.1}

2015-04-02  ExtraTrees-b41e774_b41e774.csv scored 0.73442, this had
new_dist = {"Wilderness_Area1": 1.0, "Wilderness_Area2": 0.1,"Wilderness_Area3": 1.0, "Wilderness_Area4": 0.1}

2015-03-24  ExtraTrees-e1c2a40_e1c2a40.csv scored 0.60178, this had 
new_dist = {"1":1.0,"2":1.0,"3":0.4,"4":0.05,"5":0.05,"6":0.1,"7":0.05}

2015-03-24  ExtraTrees-5c351c4_5c351c4.csv scored 0.60883, this had 
new_dist = {"1":1.0,"2":1.0,"3":0.2,"4":0.2,"5":0.2,"6":0.2,"7":0.2}

2015-03-24  ExtraTrees-939ef38_939ef38.csv scored 0.57076, this had 
new_dist = {"1":1.0,"2":1.0,"3":0.4,"4":0.4,"5":0.4,"6":0.4,"7":0.4}

2015-03-24  KNeighbours-7e03b76_7e03b76.csv scored 0.57953

2015-03-24  ExtraTrees-67f13f3_67f13f3 scored 0.62617, this one had biased training inputs.
new_dist = {"1":1.0,"2":1.0,"3":0.1,"4":0.1,"5":0.1,"6":0.1,"7":0.1}. This score is lower than 
my calculation of 0.74

2015-03-21 Third submission scored 0.42309 - even worse! This used KNeighbours

2015-03-21 Second submission scored 0.33295 - even worse! This used LinearSVC

2015-03-21  First submission scored 0.46214 - much lower than expected, some explanation here:
http://www.kaggle.com/c/forest-cover-type-prediction/forums/t/10708/validation-versus-lb-score
zip/zip-csv makes no difference. 
forest-cover-type-Ian-Hopkinson-2015-03-21.csv
classifier-pickles/forest_cover_classifier_ExtraTrees_a1e9e94cc.joblib.pkl





My classifiers are scoring approximately half on the Kaggle leader board as I am getting on the
training set - this is likely because of a very strong bias to types one and two in the test set

