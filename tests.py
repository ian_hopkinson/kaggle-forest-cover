#!/usr/bin/env python
# encoding: utf-8

from __future__ import unicode_literals
import unittest

from collections import OrderedDict
from nose.tools import assert_equal, raises
from utilities import (biased_subsample_categorical,
                       biased_subsample_continuous,
                       biased_subsample_onehot,
                       field_histogram, 
                       sort_dict_by_value)

from collections import Counter


class biased_subsample_continuousTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.column = [0.5, 1.5, 2.5, 3.5]
        cls.new_dist = {"bins":[0.0, 1.0, 2.0, 3.0, 4.0],
                        "dist":[1.0, 1.0, 1.0, 1.0]}
        cls.ones = [1.9] * 10
   
    def test_keep_all(self):
        self.new_dist['dist'] = [1.0, 1.0, 1.0, 1.0]
        expected = [0, 1, 2, 3]
        observed = biased_subsample_continuous(self.column, self.new_dist)
        assert_equal(expected, observed)

    def test_drop_all_of_one(self):
        self.new_dist['dist'] = [0.0, 1.0, 1.0, 1.0]
        expected = [1, 2, 3]
        observed = biased_subsample_continuous(self.column, self.new_dist)
        assert_equal(expected, observed)

    def test_drop_half(self):
        self.new_dist['dist'] = [1.0, 0.5, 1.0, 1.0]
        observed = biased_subsample_continuous(self.ones, self.new_dist)
        assert_equal(5, len(observed))

    def test_drop_90(self):
        self.new_dist['dist'] = [1.0, 0.1, 1.0, 1.0]
        observed = biased_subsample_continuous(self.ones, self.new_dist)
        assert_equal(1, len(observed))

class biased_subsample_categoricalTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.column = [1, 2, 3, 4]
        cls.ones = [1] * 1000
    
    def test_keep_all(self):
        new_dist = {1:1.0, 2:1.0, 3:1.0, 4:1.0}
        expected = [0, 1, 2, 3]
        observed = biased_subsample_categorical(self.column, new_dist)
        assert_equal(expected, observed)

    def test_drop_all_of_one(self):
        new_dist = {1:0.0, 2:1.0, 3:1.0, 4:1.0}
        expected = [1, 2, 3]
        observed = biased_subsample_categorical(self.column, new_dist)
        assert_equal(expected, observed)

    def test_drop_half(self):
        new_dist = {1:0.5}
        observed = biased_subsample_categorical(self.ones, new_dist)
        assert_equal(500, len(observed))

    def test_drop_90(self):
        new_dist = {1:0.1}
        observed = biased_subsample_categorical(self.ones, new_dist)
        assert_equal(100, len(observed))

class biased_subsample_onehotTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.data = [[1, 0, 0, 0, 1],
                    [0, 1, 0, 0, 2],
                    [0, 0, 1, 0, 3],
                    [0, 0, 0, 1, 4]]

        cls.notonehot = [[1, 0, 0, 0, 1],
                        [0, 1, 0, 0, 2],
                        [1, 0, 1, 0, 3],
                        [0, 0, 0, 1, 4]]

        cls.feature_names = {"a0": 0, "a1": 1, "a2":2, "a3":3}


    def test_onehot_drop_all(self):
        new_dist = {"a0": 0.0, "a1": 0.0, "a2":0.0, "a3":0.0}
        observed = biased_subsample_onehot(self.data, new_dist, self.feature_names)
        expected = []
        assert_equal(set(expected), set(observed))

    def test_onehot_drop_one(self):
        new_dist = {"a0": 1.0, "a1": 1.0, "a2":0.0, "a3":1.0}
        observed = biased_subsample_onehot(self.data, new_dist, self.feature_names)
        expected = [0, 1, 3]
        assert_equal(set(expected), set(observed))

    def test_onehot_keep_all(self):
        new_dist = {"a0": 1.0, "a1": 1.0, "a2":1.0, "a3":1.0}
        observed = biased_subsample_onehot(self.data, new_dist, self.feature_names)
        expected = [0, 1, 2, 3]
        assert_equal(set(expected), set(observed))

    @raises(TypeError)
    def test_not_onehot(self):
        new_dist = {"a0": 1.0, "a1": 1.0, "a2":1.0, "a3":1.0}
        observed = biased_subsample_onehot(self.notonehot, new_dist, self.feature_names)

class feature_histogramTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.data_ints = [1, 1, 2, 2, 3, 3, 4]
        cls.data_strs = ["a", "a", "b", "b", "b"]
        cls.onehot = [[1, 0, 0, 0, 1],
                    [0, 1, 0, 0, 2],
                    [0, 0, 1, 0, 3],
                    [0, 0, 0, 1, 4],
                    [0, 0, 0, 1, 5]]
        cls.feature_names = {"a0": 0, "a1": 1, "a2":2, "a3":3}


    def test_simple_case_ints(self):
        observed = field_histogram(self.data_ints)
        expected = Counter({1: 2, 2: 2, 3:2, 4:1})
        assert_equal(expected, observed)

    def test_simple_case_strs(self):
        observed = field_histogram(self.data_strs)
        expected = Counter({"a": 2, "b": 3})
        assert_equal(expected, observed)

    def test_onehot_case(self):
        observed = field_histogram(self.onehot, onehot_set=self.feature_names)
        expected = Counter({"a0": 1, "a1": 1, "a2": 1, "a3": 2})
        assert_equal(expected, observed)

    @raises(KeyError)
    def test_expected_category_count(self):
        observed = field_histogram(self.data_strs, n_categories_expected=3)
        expected = Counter({"a": 2, "b": 3})
        assert_equal(expected, observed)


def test_sort_dictionary():
    unsorted = {"a": 1, "b":0, "c": 2}
    observed = sort_dict_by_value(unsorted)
    expected = OrderedDict([("b", 0),("a", 1),("c", 2)])
    assert_equal(expected, observed)