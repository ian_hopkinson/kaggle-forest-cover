#!/usr/bin/env python
# encoding: utf-8
from __future__ import unicode_literals

import codecs
import csv
import logging
import math
import numpy
import operator
import os
import random 
import subprocess
import sys

from collections import OrderedDict, Counter

logging.basicConfig(level=logging.DEBUG)

def field_histogram(data, n_categories_expected=None, onehot_set=None):
    # If we are given a column then count frequency of items in the column,
    # the key is the column content
    # Warn when column 
    # if we are given a onehot_set then the key is the field name
    # We can provide n_categories_expected to verify how many entries we 
    # expected  
    field_counter = Counter()

    print(numpy.shape(data))
    if onehot_set is None:
        for row in data:
            field_counter[row] += 1
    else:
        for row in data:
            for key in onehot_set.keys():
                if row[onehot_set[key]] == 1:
                    field_counter[key] += 1

    if (n_categories_expected is not None) and (len(field_counter) != n_categories_expected):
        raise KeyError("Number of categories found '{}' was not expected '{}'".format(len(field_counter),n_categories_expected))
    return field_counter

def biased_subsample_continuous(column, new_dist):
    """
    new_dist must contain a histogram comprised of bin edges (n+1) and fractions
    i.e. new_dist = {bins: [0, 1.0, 2.0, 3.0, 5.0], dist: [1, 0.1, 0.1, 0.1]}

    """
    index_array = []
    bin_indices = {}

    column_binned = numpy.digitize(column, new_dist['bins']) - 1
    for i, c in enumerate(column_binned):
        if c in bin_indices.keys():
            bin_indices[c].append(i)
        else:
            bin_indices[c] = [i]

    for c in bin_indices.keys():
        keep_frac = new_dist['dist'][c]
        if keep_frac == 1.0:
            index_array.extend(bin_indices[c])
        elif keep_frac == 0.0:
            continue
        else:
            required_length = int(len(bin_indices[c]) * keep_frac)
            random.shuffle(bin_indices[c])
            index_array.extend(bin_indices[c][:required_length])

    return index_array


def biased_subsample_onehot(data, new_dist, column_names, method="exact"):
    """
    new_dist must contain entries for all options in the onehot set

    """
    index_array = []
    # For each key in new_dist determine column, call biased_subsample_categorical
    for key in new_dist.keys():
        col = column_names[key]
        onehot_dist = {1: new_dist[key], 0: 0.0}
        column = [row[col] for row in data] 
        onehot_indices = biased_subsample_categorical(column, onehot_dist, method="exact")    
        index_array.extend(onehot_indices)

    for row in data:
        sum_ = 0.0
        for key in new_dist.keys():
            col = column_names[key]
            #print(row[col])
            sum_ = sum_ + row[col]
        if sum_ > 1.0:
            print("Sum_: {}".format(sum_))
            print("new_dist: {}".format(new_dist))
            print("column_names: {}".format(column_names))
            print("row: {}".format(row))
            raise TypeError("Supplied fields are not a onehot set")

    return index_array
    
def biased_subsample_categorical(column, new_dist, method="exact"):
    """
    Assume an input column `column` with some distribution of a categorical
    variable. This function will return a set of indices describing a 
    column with a biased subsample. The new_dist dictionary contains a lookup
    with the fraction of a category kept in the subsample (i.e. 1.0 means keep all, 
    0 means drop all and 0.1 is keep 1 in 10). For example:
    new_dist = {1:1.0,2:1.0,3:0.1,4:0.1,5:0.1,6:0.1,7:0.1}
    if a category is not found in new_dict then it is always kept.
    The "exact" method shuffles the array and takes a fractional slice.
    The "rand" method randomly accepts or rejects an entry depending on threshold,
    this means it does not guarentee the output proportions
    """

    index_array = []

    if method == "rand":
        for i, c in enumerate(column):
            keep_frac = new_dist.get(c, 1.0)
            if keep_frac == 1.0:
                index_array.append(i)
            elif keep_frac == 0.0:
                continue
            elif random.random() < keep_frac:
                index_array.append(i)
    elif method == "exact":
        category_indices = {} 
        for i, c in enumerate(column):
            if c in category_indices.keys():
                category_indices[c].append(i)
            else:
                category_indices[c] = [i]

        for c in category_indices.keys():
            keep_frac = new_dist.get(c, 1.0)
            if keep_frac == 1.0:
                index_array.extend(category_indices[c])
            elif keep_frac == 0.0:
                continue
            else:
                required_length = int(len(category_indices[c]) * keep_frac)
                random.shuffle(category_indices[c])
                index_array.extend(category_indices[c][:required_length])

    return index_array

def git_uncommitted_changes(filename, repo_dir):
    if filename == ' ' or filename == '':
        filename = '--'
    cmd = ['git', 'diff', filename]
    pr = subprocess.Popen(cmd,                  
            cwd=repo_dir, 
            stdout=subprocess.PIPE, 
            stderr=subprocess.PIPE, 
            shell=False)
    (out, error) = pr.communicate()

    if len(error) > 0:
        print(error)

    if len(out) > 0:
        return True
    else:
        return False

def git_sha(repo_dir):
    cmd = ['git','rev-parse','--short', 'HEAD']
    pr = subprocess.Popen(cmd,                  
            cwd=repo_dir, 
            stdout=subprocess.PIPE, 
            stderr=subprocess.PIPE, 
            shell=False)
    (out, error) = pr.communicate()

    if len(error) > 0:
        print(error)

    return out.strip()

def sort_dict_by_value(unordered_dict):
    sorted_dict = sorted(unordered_dict.items(), key=operator.itemgetter(1))
    return OrderedDict(sorted_dict)

def pretty_print_dict(dictionary):
    # print("Feature names: {}\n".format(feature_names))
    WIDTH = 160
    # find longest feature_name
    max_width = max([len(key) for key in dictionary.keys()]) + 2
    # find out how many of longest feature name fit in 80 characters
    n_columns = math.floor(WIDTH/(max_width + 7))
    # Build format string
    fmt = "%{}s:%3d".format(max_width)
    # feed feature_names into format string
    report = ''
    i = 1
    for key, value in dictionary.items():
        report = report + fmt % (key,value)
        if (i % n_columns) == 0:
            report = report + "\n"
        i = i + 1

    print(report)
    return report

def write_dictionary(filename, data):    
    logging.info("Appending data to {}".format(filename))    
    keys = data[0].keys()

    newfile =  not os.path.isfile(filename)

    with open(filename, 'ab') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        if newfile:
            dict_writer.writeheader()
        dict_writer.writerows(data)

if __name__ == "__main__":
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)

    