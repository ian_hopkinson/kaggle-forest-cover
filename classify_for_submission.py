#!/usr/bin/env python
# encoding: utf-8
from __future__ import unicode_literals

import codecs
import sys
from time import strftime, gmtime

from sklearn.externals import joblib
from forest_type_classifier import load_forest_data, write_dictionary

from collections import OrderedDict

from utilities import git_sha, git_uncommitted_changes
from submission_validate import check


def save_report(report):
    log_row = OrderedDict([
        ('time', report['datetime']),
        ('sha', report['sha']),
        ('classifier_file', report['classifier_file']),
        ('output_file', report['output_file']),
        ('test_run', report['test_run'])
    ])

    write_dictionary('output/submission_log.csv', [log_row])

def make_output_filename(classifier_file):
    submit_sha = git_sha('.')
    filename = "{}_{}.csv".format(classifier_file.replace('.joblib.pkl', ''), submit_sha)
    return filename

def main(argv=None):
    if argv is None:
        argv = sys.argv
    arg = argv[1:]
    
    if len(arg) == 0:
        print("Available commandlines:\n")
        print("classify_for_submission.py [classifier filename]")
        print("classify_for_submission.py [classifier filename] test")
        return
    elif len(arg) == 1:
        chunk_size = 50000
        test = False
        classifier_file = arg[0]
    else:
        chunk_size = 100
        test = True
        classifier_file = arg[0]

    SUBMISSION_ROW = OrderedDict([
        ('Id', None),
        ('Cover_Type', None),
    ])

    if git_uncommitted_changes('--', '.'):
        print("There are uncommitted changes in the repo please commit and retry")
        return

    report = {}
    report['datetime'] = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    report['sha'] = git_sha('.')
    report['classifier_file'] = classifier_file
    output_file = make_output_filename(classifier_file)
    report['output_file'] = output_file
    report['test_run'] = test

    # Loop over test data set in chunks to avoid MemoryError in data rescaling
    still_data = True
    offset = 0
    while still_data:
        print("Processing chunk at offset = {}".format(offset))
        output = []
        # load the test data - need to process in the same way as for building classifier
        data, target, target_names, feature_names, Id = load_forest_data("input/test.csv", offset=offset, limit=chunk_size)
        print("data length: {}".format(len(data)))
        if len(Id)==0:
            still_data = False
            print("Exiting with Id length of zero")
            break

        # load the classifier - classifier loaded after data for memory limit reasons
        classifier = joblib.load("classifier-pickles/{}".format(classifier_file))
        
        # Test feature_names are in the same order in the classifier as the data
        # Do this in forest_type_classifier

        # Apply classifier to test data
        target_pred = classifier.predict(data)
        # output test data in required format
        output_tuples = zip(Id, target_pred)

        for r in output_tuples:
            row = SUBMISSION_ROW.copy()
            row['Id'] = r[0]
            row['Cover_Type'] = r[1]
            output.append(row)

        write_dictionary('tmp/{}'.format(output_file), output)
        del classifier, output, output_tuples, data, target, target_names, target_pred, Id
        offset = offset + chunk_size
        # Early exit for testing
        if test and offset > 100:
            print("Exiting early because test is True")
            still_data = False

    check('tmp/{}'.format(output_file))
    save_report(report)

if __name__ == "__main__":
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    main()
    