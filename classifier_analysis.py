#!/usr/bin/env python
# encoding: utf-8
from __future__ import unicode_literals

import codecs
import sys

from forest_type_classifier import load_forest_data
from sklearn.externals import joblib
from utilities import sort_dict_by_value, pretty_print_dict

def main(argv=None):
    if argv is None:
        argv = sys.argv
    arg = argv[1:]
    
    if len(arg) == 0:
        print("Available commandlines:\n")
        print("classifier_analysis.py [classifier filename]")
        return
    else:
        classifier_file = arg[0]

    report = {}
    data, target, target_names, feature_names, Id = load_forest_data("input/train.csv")
    report['feature_names'] = sort_dict_by_value(feature_names)
    pretty_print_dict(report['feature_names'])

    classifier = joblib.load("classifier-pickles/{}".format(classifier_file))

    #print dir(classifier)
    #print(classifier.feature_importances_)

    for key in report['feature_names'].keys():
        value = classifier.feature_importances_[feature_names[key]] 
        print("{} : {}".format(key, value))


if __name__ == "__main__":
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    main()
    