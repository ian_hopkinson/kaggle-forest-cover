#!/usr/bin/env python
# encoding: utf-8
from __future__ import unicode_literals

import codecs
import csv
import logging
import math
import numpy
import sys

from time import strftime, gmtime, time

from collections import OrderedDict

from sklearn import datasets
from sklearn.cross_validation import train_test_split
from sklearn.dummy import DummyClassifier

from sklearn.metrics import classification_report
from sklearn.feature_extraction import DictVectorizer
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support
from sklearn import preprocessing

from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC
from sklearn.svm import NuSVC
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.pipeline import Pipeline
from sklearn.externals import joblib

from utilities import (git_uncommitted_changes, git_sha, 
                       biased_subsample_categorical,
                       biased_subsample_continuous,
                       biased_subsample_onehot,
                       field_histogram,
                       sort_dict_by_value,
                       pretty_print_dict,
                       write_dictionary)

logging.basicConfig(level=logging.DEBUG)

def load_iris_data():
    iris = datasets.load_iris()
    data = iris.data
    target = iris.target
    target_names = iris.target_names
    feature_names = iris.feature_names

    return data, target, target_names, feature_names

def get_forest_target_names():
    target_names_file = 'input/tree_types.csv'
    target_names = []
    with open(target_names_file, 'r') as csvfile:
        reader = csv.reader(csvfile)
        reader.__next__()
        for row in reader:
            target_names.append(str(row[0]) + row[1])

    return target_names

def load_forest_data(filename, offset=0, limit=float('inf')):
    raw_data = []
    target = []
    Id = []
    with open(filename, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for i, row in enumerate(reader):
            if i >= offset and i < offset + limit:
            #if limit is None or i < limit:
                raw_data.append(row)
                Id.append(row['Id'])
                if "Cover_Type" in row:
                    target.append(row['Cover_Type'])
    # Return early if we have no data
    if len(raw_data)==0:
        return [], [], [], [], []        
    # Process raw_data to make feature sets (modify conver to array)
    data, feature_names = process_raw(raw_data)
    # Get target_names
    target_names = get_forest_target_names()

    return data, target, target_names, feature_names, Id

def process_raw(raw_data):
    feature_keys_in = ['Elevation', 'Slope', 'Aspect',
                       'Wilderness_Area1', 'Wilderness_Area2', 
                       'Wilderness_Area3', 'Wilderness_Area4',
                       'Horizontal_Distance_To_Hydrology',
                       'Vertical_Distance_To_Hydrology',
                       'Horizontal_Distance_To_Roadways', #This causes a memory error
                       'Horizontal_Distance_To_Fire_Points', #This causes a memory error
                       #'Hillshade_9am', 
                       #'Hillshade_Noon', 
                       #'Hillshade_3pm',
                       'Soil_Type29', 'Soil_Type19', 'Soil_Type1', 'Soil_Type2',
                       'Soil_Type3', 'Soil_Type4', 'Soil_Type5', 'Soil_Type6'
                    ]
    feature_keys_out = [#'Horizontal_Distance_To_Hydrology',
                        #'Vertical_Distance_To_Hydrology',
                        #'Horizontal_Distance_To_Roadways',
                        #'Horizontal_Distance_To_Fire_Points',
                        'Hillshade_9am', 'Hillshade_Noon', 'Hillshade_3pm',
                        'Cover_Type', 'Id'
                    ]
    soil_types = ["Soil_Type{}".format(x) for x in range(1,42)]
    feature_keys_out.extend(soil_types)
    # feature_names = []
    for row in raw_data:
        for i, key in enumerate(list(row.keys())):
            if key in feature_keys_out:
                row.pop(key, None)
            else:
                row[key] = float(row[key])
            # if key not in feature_keys_in: # and not key.startswith("Soil_Type"):
            #     row.pop(key, None)
            # else:
            #     row[key] = float(row[key])

    #for key in raw_data[0].keys():
    #    feature_names.append(key)

    v = DictVectorizer(sparse=False)
    data = v.fit_transform(raw_data)
    feature_names = v.vocabulary_

    #Onehot variables are Soil_Type* and Wilderness_Area*
    feature_list = []

    for feature in feature_names.keys():
        if feature.startswith("Wilderness_Area") or feature.startswith("Soil_Type"):
            continue
        else:
            feature_list.append(feature_names[feature])

    _ = preprocessing.scale(data[:,feature_list], copy=False)

    return data, feature_names

def make_dummy_classifier(features, classes):
    dummy_classifier = DummyClassifier()
    dummy_classifier.fit(features, classes)
    return dummy_classifier

def make_classifier(features, classes, classifier_type):
    if classifier_type == 'LinearSVC':
        classifier = LinearSVC()
    elif classifier_type == 'NuSVC':
        classifier = NuSVC()
    elif classifier_type == 'KNeighbours':
        classifier = KNeighborsClassifier()
    elif classifier_type == 'DecisionTree':
        classifier = tree.DecisionTreeClassifier()
    elif classifier_type == 'AdaBoost':
        classifier = AdaBoostClassifier(n_estimators=100)
    elif classifier_type == 'Pipeline1':
        classifier = Pipeline([
            ('feature_selection', LinearSVC(penalty="l1", dual=False)),
            ('classification', ExtraTreesClassifier(n_estimators=1000, max_depth=None,
                                                    max_features=7,
                                                    min_samples_split=2, random_state=0,
                                                    bootstrap=False))
            ])
    elif classifier_type == 'ExtraTrees':
        classifier = ExtraTreesClassifier(n_estimators=200, max_depth=None,
                                                    max_features=7,
                                                    min_samples_split=2, random_state=0,
                                                    bootstrap=False)
    else:
        logging.info("Classifier {} not recognised, using dummy".format(classifier_type))
        classifier = DummyClassifier()

    classifier.fit(features, classes) 
    return classifier

def print_report(report):
    print("\n{} classifier report\n=====================".format(report['classifier_type']))
    print(report['classifier'])
    print("Data size: {}".format(report['n_total']))
    print("Training set size: {}".format(report['n_training']))
    print("Number of features: {}".format(report['n_features']))
    print("Training time: {0:0.2f} seconds".format(report['t_training']))
    print("Feature_name:column\n")
    pretty_print_dict(report['feature_names'])
    print(report['classifier_report'])
    # Confusion matrix
    print("Confusion matrix\n================\n")
    print(report['cm'])
    
    # plt.matshow(report['cm'])
    # plt.title('Confusion matrix')
    # plt.colorbar()
    # plt.ylabel('True label')
    # plt.xlabel('Predicted label')
    # plt.show()

    print("\nDummy classifier report\n=======================")
    print(report['dummy_report'])

def save_report(report):
    log_row = OrderedDict([
        ('time', report['datetime']),
        ('sha', report['sha']),
        ('classifier_type', report['classifier_type']),
        ('training_time(s)', "{0:0.2f}".format(report['t_training'])),
        ('training_set_size', report['n_training']),
        ('number_of_features', report['n_features']),
        ('avg_f1', "{0:0.2f}".format(report['avg_f1'])),
        ('avg_dummy_f1', "{0:0.2f}".format(report['avg_dummy_f1'])),
        ('classifier_details', report['classifier'] ),
        ('feature_names', report['feature_names']),
        ('bias_function', report['bias_function']),
    ])

    write_dictionary('output/run_log.csv', [log_row])

def check_feature_names(train_feature_names):
    _, _, _, test_feature_names, _ = load_forest_data("input/test.csv", limit=100)

    keys = train_feature_names.keys()
    for key in keys:
        assert train_feature_names[key] == test_feature_names[key]
    #print train_feature_names
    #print test_feature_names
    return

def main(argv=None):
    test = True
    if argv is None:
        argv = sys.argv
    arg = argv[1:]

    if len(arg) == 0:
        print("Available commandlines:\n")
        print("forest_type_classifier.py LinearSVC")
        print("forest_type_classifier.py NuSVC")
        print("forest_type_classifier.py KNeighbours")
        print("forest_type_classifier.py DecisionTree")
        print("forest_type_classifier.py AdaBoost")
        print("forest_type_classifier.py Pipeline1")
        print("forest_type_classifier.py ExtraTrees")
        return
    elif len(arg)==1:
        test = True
        classifier_type = arg[0]
    else:
        test = True
        classifier_type = arg[0]

    if git_uncommitted_changes('--', '.') and not test:
        print("There are uncommitted changes in the repo please commit and retry")
        return

    report = {}
    report['datetime'] = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    report['sha'] = git_sha('.')

    # Import data and convert to numpy array
    #data, target, target_names,feature_names = load_iris_data()
    data, target, target_names, feature_names, Id = load_forest_data("input/train.csv")

    # subsample data based on target class
    #new_dist = {"1":1.0,"2":1.0,"3":0.1,"4":0.1,"5":0.1,"6":0.1,"7":0.1}
    #report['bias_function'] = new_dist
    #subsample_indices = biased_subsample_categorical(target, new_dist)
    #data = [data[i] for i in subsample_indices]
    #target = [target[i] for i in subsample_indices]
    #Id = [Id[i] for i in subsample_indices]

    # subsample data based on "onehot" categories
    # Aim is a particular distribution 
    # new_dist = {"Wilderness_Area1": 1.0, "Wilderness_Area2": 0.1,
    #             "Wilderness_Area3": 1.0, "Wilderness_Area4": 0.1}
    
    

    # onehot_set = {"Wilderness_Area1": feature_names['Wilderness_Area1'], 
    #               "Wilderness_Area2": feature_names['Wilderness_Area2'],
    #               "Wilderness_Area3": feature_names['Wilderness_Area3'], 
    #               "Wilderness_Area4": feature_names['Wilderness_Area4']}

    # subsample_indices = biased_subsample_onehot(data, new_dist, feature_names)
    # print("Feature distribution pre-subsampling")
    # print(field_histogram(data, onehot_set=onehot_set))
    # subsample_indices = biased_subsample_continuous(elevation, new_dist)
    
    # data = [data[i] for i in subsample_indices]
    # target = [target[i] for i in subsample_indices]
    # Id = [Id[i] for i in subsample_indices]
    # print("Feature distribution post-subsampling")
    # print(field_histogram(data, onehot_set=onehot_set))

    step = 200.0
    start = 1800.0
    bins = [start + x * step for x in range(0,12)]
    new_dist = {"bins":bins,
                "dist":[0.1, 0.1, 0.1, 0.1, 1.0, 1.0, 1.0, 1.0, 0.1, 0.1, 0.1]}
    elevation = data[:, feature_names['Elevation']]
    subsample_indices = biased_subsample_continuous(elevation, new_dist)
    
    data = [data[i] for i in subsample_indices]
    target = [target[i] for i in subsample_indices]
    Id = [Id[i] for i in subsample_indices]
    

    report['bias_function'] = new_dist                    
   

    # More reporting data
    report['n_total'] = len(data)
    report['feature_names'] = sort_dict_by_value(feature_names)
    report['target_names'] = target_names

    # Compare feature names in test dataset - this destroys data
    check_feature_names(feature_names)
    # Make training and test datasets
    (data_train,
     data_test,
     target_train,
     target_test) = train_test_split(data, target, train_size=0.99) #, train_size=1000, test_size=1000)
    report['n_training'] = len(data_train)
    report['n_features'] = len(data_train[0])
    # Make classifier
    start = time()
    classifier = make_classifier(data_train, target_train, classifier_type)
    report['t_training'] = time() - start
    target_pred = classifier.predict(data_test)

    report['classifier'] = ' '.join(repr(classifier).replace("\n","").split())
    report['classifier_type'] = type(classifier).__name__

    report['cm'] = confusion_matrix(target_test, target_pred)

    # Make Dummy classifier
    dummy_classifier = make_dummy_classifier(data_train, target_train)
    dummy_pred = dummy_classifier.predict(data_test)
    # Report on classifier performance
    report['dummy_report'] = classification_report(
        target_test,
        dummy_pred, target_names = target_names)

    report['classifier_report'] = classification_report(
        target_test,
        target_pred, target_names = target_names)

    p, r, f1, s = precision_recall_fscore_support(target_test, target_pred)
    report['f1'] = f1
    report['avg_f1'] = sum(f1)/len(f1)
    del p, r, f1, s
    p, r, f1, s = precision_recall_fscore_support(target_test, dummy_pred)
    report['avg_dummy_f1'] = sum(f1)/len(f1)
    # print p, r, f1, s

    # Print report
    print_report(report)
    if not test:
        # Save classifier
        joblib.dump(classifier, 
            "classifier-pickles/{}-{}.joblib.pkl"
            .format(classifier_type, report['sha']),compress=1)
        # Save report to log
        save_report(report)
    
if __name__ == "__main__":
    main()
    